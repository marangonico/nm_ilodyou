#### Script: ILODYou by nicotum 
##### A modified version of Auto_LOD 1.3 with permission from oe3gsu and maydayc (thank you guys for your kindness!!!)

###### Updating the script:
1. Copy the script nm_ILODYou.lua under _"resources/plugins/FlyWithLUA/scripts"_
2. Copy the script LIP.lua under _"resources/plugins/FlyWithLUA/modules"_
3. If you have modified your nm_ILODYou.ini, make a backup copy before
copying the file nm_ILODYou.ini under
_"resources/plugins/FlyWithLUA/scripts"_, then look at new available
settings (if any).

###### First Installation:
1. Copy the script nm_ILODYou.lua under _"resources/plugins/FlyWithLUA/scripts"_
2. Copy the file nm_ILODYou.ini under _"resources/plugins/FlyWithLUA/scripts"_
3. Copy the script LIP.lua under _"resources/plugins/FlyWithLUA/modules"_

###### What this script does:
- tries to keep FPS under control handling the LOD value (can be disabled)
- tries to keep FPS under control handling the HDX value (can be disabled)
- handles the DSF Visibility in order to eliminate the mud effect and keep FPS under control (can be disabled)
- handles the fog_be_gone dataref in order to keep FPS under control and to simulate a more accurate haze (can be disabled)
- Above a certain height, you can tell the script to turn off completely all objects. Useful when flying over orthophotos (can be disabled)

This script is provided as is.
It is a tool modified/developed for personal use, so consider that:
- it's the result of personal needs and curiosity.
- this is not magic.. it tries to keep fps high enough at the expense of gfx quality.. your low end pc will remain the same.
- if your fps are locked to 30fps, this script is probably useless if not harmful.

To customize it, edit the file nm_ILODYou.ini, it is almost self-explanatory.

The script displays 4 boxes:
1. **Title box:**

    click inside it to hide/show extended info boxes
2. **FPS box:**

    click inside it to switch from auto to manual mode (red background in auto-mode)
3. **Info box:**

    click inside it to switch the type of info displayed (Average FPS, fog_be_gone dataref, max_dsf_vis_ever dataref)
4. **LOD/HDR box:**

    On the first position, this box shows the actual LOD value and HDR setting.
    Clicking inside it, you can access two more positions: manual LOD and HDR.
    With the mouse positioned over these values you can lower of raise them, but this makes sense only if you want to go manual.
    To go manual, switch mode clicking over the fps box first until it becomes transparent.

You can tell the script where to show the boxes.
See POS_X and POS_Y settings under [UI] section
(example: POS_X=LEFT and POS_Y=BOTTOM)

You can hide/show the script boxes in 2 ways:
1. Using the macro menu under _"plugins->FlyWithLUA Macros->nmILODYou Show Hide window"_
2. Binding a key/button to the custom command _"FlyWithLua/nm_ILODYou/show_hide"_

**Notes & Tips:**
- default low value for HDR is FXAA.
Since LOD is always winning over HDR, if you don't want to go so low on
HDR, set MIN_VALUE under HDR section at least equal to 3 (__ __HDR_2xSSAA_FXAA__ __= 3).

Constructive feedback is welcome.

Have fun.

**Changes**
- Version 1.5:

    1. From X-Plane 11.30+, changing HDR settings on the fly generates artifacts
	   The sim now caches a bunch of info about the art controls once so it can 
	   pre-init a bunch of graphics structures... that's why the art controls 
	   aren't having immediate effect
    2. If XEnviro is detected DSF Visibility and "fog be gone" are not touched

- Version 1.4:

    1. Now reads HDR.MIN_VALUE and HDR.MAX_VALUE from ini file
    2. Fixes and error in .ini file format version "1", where
    HDR.MIN_VALUE and HDR.MAX_VALUE were reversed
    3. More FPS friendly settings in the example ini file
    4. New informative LOD+HDR box

- Version 1.3:

    1. New LOD_THRESHOLD_BEFORE_LOWERING_HDR setting.
    If specified this is the LOD value above which the script will
    evaluate a lowering of HDR settings
    This setting will give some priority to HDR over LOD.

- Version 1.2:

    1. You can tell the script where to show the boxes.
    See POS_X and POS_Y settings under [UI] section
    (example: POS_X=LEFT and POS_Y=BOTTOM)

**Credits:**
LIP.lua distributed under MIT licence by Carreras Nicolas
The original Auto_LOD by oe3gsu and maydayc
