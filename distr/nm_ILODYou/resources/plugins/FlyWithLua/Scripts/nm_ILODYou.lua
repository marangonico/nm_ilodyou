--Script: ILODYou by nicotum
--Modified version of Auto_LOD 1.3 with permission from oe3gsu and maydayc 

local app = {name = 'nm_ILODYou', caption = 'ILODYou', version = '1.5'}

logMsg('Init script ' .. app['caption'] .. ' version ' .. app['version'])

local LIP = require 'LIP';
local CONFIG = LIP.load(SCRIPT_DIRECTORY .. app['name'] .. '.ini')

-----------------------------------------------------------------------------
function getValue(set, key, default_value)

	if set == nil then
		return default_value
	end

    if set[key] ~= nil then
		return set[key]
	else
		return default_value
	end

end

-----------------------------------------------------------------------------
function setDefaultValue(set, key, default_value)

    if set[key] == nil then
		set[key] = default_value
	end

end

-----------------------------------------------------------------------------
local __FEET_2_METERS__ = 3.28084

--LIP.save(SCRIPT_DIRECTORY .. app['name'] .. '-verify-ini.txt', CONFIG)

setDefaultValue(CONFIG.HDR, "LOD_THRESHOLD_BEFORE_LOWERING_HDR", CONFIG.LOD.__LOD_WORLD_VISIBILITY_VERYHIGH__)

DataRef("lod_bias_rat", "sim/private/controls/reno/LOD_bias_rat", "writable")
if CONFIG.LOD.SET_INITIAL_VALUE then
	-- sets the initial LOD World Visibility Value
	lod_bias_rat = CONFIG.LOD.INITIAL_VALUE	
end

lod_control_mode_auto = CONFIG.LOD.CHANGE 
lod_show_window = true

local lod_agl_threshold = 0
if CONFIG.LOD.AGL_THRESHOLD_FEET > 0 then
	-- threshold beyond which the LOD is set to 9.9
	lod_agl_threshold = CONFIG.LOD.AGL_THRESHOLD_FEET / __FEET_2_METERS__		
else
	lod_agl_threshold = 99999999
end

local lod_bias_rat_below_agl_threshold = -1

local hdr_post_aa_only = XPLANE_VERSION >= 11300
hdr_post_aa_only = false
local hdr_post_aa_text = ''

--local SOFT_CLOUDS_ENABLED = false


-----------------------------------------------------------------------------
function calc_fps_down_threshold()
	
	local threshold = CONFIG.LOD.FPS_MIN + CONFIG.LOD.FPS_MIN / 10 + 1
	if threshold > CONFIG.LOD.FPS_MAX then threshold = CONFIG.LOD.FPS_MAX end
	return threshold
	
end
-----------------------------------------------------------------------------

-- fps threshold for average fps beyond which the script tries to lower the LOD value
local lod_fps_down_threshold = calc_fps_down_threshold()	

require("graphics")

DataRef("frame_rate_period", "sim/operation/misc/frame_rate_period", "readonly")   -- 1/frame_rate_period = FPS

DataRef("fsaa_ratio_x", "sim/private/controls/hdr/fsaa_ratio_x", "writable")
DataRef("fsaa_ratio_y", "sim/private/controls/hdr/fsaa_ratio_y", "writable")
DataRef("use_post_aa", "sim/private/controls/hdr/use_post_aa", "writable")

DataRef("is_sim_paused", "sim/time/paused", "readonly")

DataRef("hgt_m", "sim/flightmodel/position/y_agl", "readonly")

dataref("fog_be_gone", "sim/private/controls/fog/fog_be_gone", "writable")

local xE_is_installed = false

if CONFIG.DSF_VISIBILITY.CHANGE then

	if XPLMFindDataRef("env/active") then
		xE_is_installed = true
		dataref("xE_is_active", "env/active", "readonly", 0)
		logMsg('detected xEnviro')
	end	
	
	dataref("view_y_meters", "sim/graphics/view/view_y", "readonly", 0)

	dataref("visibility_reported_m", "sim/weather/visibility_reported_m", "readonly")
	dataref("max_dsf_vis_ever", "sim/private/controls/skyc/max_dsf_vis_ever", "writable")
	dataref("min_dsf_vis_ever", "sim/private/controls/skyc/min_dsf_vis_ever", "readonly")
	
	set("sim/private/controls/skyc/min_dsf_vis_ever", CONFIG.DSF_VISIBILITY.DSF_VIS_EVER_THRESHOLD)
	
end

local __DISPLAY_MAX__ = 4
local __DISPLAY_LOD__ = 4
local __DISPLAY_INFO__ = 3
local __DISPLAY_FPS__ = 2
local __DISPLAY_TITLE__ = 1

local __DISPLAY_LOD_MODE_LOD_HDR__ = 1
local __DISPLAY_LOD_MODE_LOD__ = 2
local __DISPLAY_LOD_MODE_HDR__ = 3
local __DISPLAY_LOD_MODE_MIN__ = __DISPLAY_LOD_MODE_LOD_HDR__
local __DISPLAY_LOD_MODE_MAX__ = __DISPLAY_LOD_MODE_HDR__

local __DISPLAY_INFO_MODE_AVERAGE_FPS__ = 1
local __DISPLAY_INFO_MODE_FOG_BE_GONE__ = 2
local __DISPLAY_INFO_MODE_MAX_DSF_VIS__ = 3
local __DISPLAY_INFO_MODE_MIN__ = __DISPLAY_INFO_MODE_AVERAGE_FPS__
local __DISPLAY_INFO_MODE_MAX__ = __DISPLAY_INFO_MODE_MAX_DSF_VIS__

local box_height = 15
local box_width = 70

local pos_x = getValue(CONFIG.UI, "POS_X", "LEFT")
local pos_y = getValue(CONFIG.UI, "POS_Y", "BOTTOM")

local box_left = 10
if pos_x == "RIGHT" then
	box_left = SCREEN_WIDTH - box_width - 1
elseif pos_x == "CENTER" then
	box_left = SCREEN_WIDTH / 2 - box_width / 2
end

local box_bottom_start = 5
if pos_y == "TOP" then
	box_bottom_start = SCREEN_HIGHT - box_height * __DISPLAY_MAX__ - 30
elseif pos_y == "CENTER" then
	box_bottom_start = SCREEN_HIGHT / 2 - box_height * __DISPLAY_MAX__ / 2
end

local box_step_y = 1
local box_bottom_last = box_bottom_start - box_step_y

local display_stop_at = __DISPLAY_MAX__
local displays = {}

for i = 1, __DISPLAY_MAX__ do 

	displays[i] = {}
	
	displays[i].left = box_left
	displays[i].right = displays[i].left + box_width
	displays[i].bottom = box_bottom_last + box_step_y
	displays[i].top = displays[i].bottom + box_height
	displays[i].height = box_height

	box_bottom_last = displays[i].top

	if i == __DISPLAY_LOD__ then

		if hdr_post_aa_only then

			displays[i][__DISPLAY_LOD_MODE_LOD_HDR__] = {title = 'LOD+AA', caption = 'L/AA:'}
			displays[i][__DISPLAY_LOD_MODE_LOD__] = {title = 'LOD', caption = 'LOD:'}
			displays[i][__DISPLAY_LOD_MODE_HDR__] = {title = 'AA', caption = 'AA:'}
			displays[i].index = __DISPLAY_LOD_MODE_LOD_HDR__
		
		else
		
			displays[i][__DISPLAY_LOD_MODE_LOD_HDR__] = {title = 'LOD+HDR', caption = 'L/H:'}
			displays[i][__DISPLAY_LOD_MODE_LOD__] = {title = 'LOD', caption = 'LOD:'}
			displays[i][__DISPLAY_LOD_MODE_HDR__] = {title = 'HDR', caption = 'HDR:'}
			displays[i].index = __DISPLAY_LOD_MODE_LOD_HDR__
		
		end
			
	elseif i == __DISPLAY_INFO__ then

		displays[i][__DISPLAY_INFO_MODE_AVERAGE_FPS__] = {title = 'AVG', caption = 'Avg:'}
		displays[i][__DISPLAY_INFO_MODE_FOG_BE_GONE__] = {title = 'FOG', caption = 'Fog:'}
		displays[i][__DISPLAY_INFO_MODE_MAX_DSF_VIS__] = {title = 'DSF', caption = 'MaxDsf:'}
		displays[i].index = __DISPLAY_INFO_MODE_AVERAGE_FPS__	

	end
	
end

local lod_fps_average_counter = 0
local lod_fps_average = 0
local lod_last_direction = ""
local lod_time = os.clock()

-----------------------------------------------------------------------------
-- HDR SETTINGS

local __HDR_NONE__ = CONFIG.HDR.__HDR_NONE__
local __HDR_FXAA__ = CONFIG.HDR.__HDR_FXAA__
local __HDR_2xSSAA_FXAA__ = CONFIG.HDR.__HDR_2xSSAA_FXAA__
local __HDR_4xSSAA__ = CONFIG.HDR.__HDR_4xSSAA__
local __HDR_4xSSAA_FXAA__ = CONFIG.HDR.__HDR_4xSSAA_FXAA__
local __HDR_8xSSAA_FXAA__ = CONFIG.HDR.__HDR_8xSSAA_FXAA__

local hdr_settings = {}
hdr_settings[__HDR_NONE__] = {}
hdr_settings[__HDR_NONE__].value = __HDR_NONE__
hdr_settings[__HDR_NONE__].text = "none"
hdr_settings[__HDR_NONE__].fsaa_ratio_x = 1
hdr_settings[__HDR_NONE__].fsaa_ratio_y = 1
hdr_settings[__HDR_NONE__].use_post_aa = 0

hdr_settings[__HDR_FXAA__] = {}
hdr_settings[__HDR_FXAA__].value = __HDR_FXAA__
hdr_settings[__HDR_FXAA__].text = "FXAA"
hdr_settings[__HDR_FXAA__].fsaa_ratio_x = 1
hdr_settings[__HDR_FXAA__].fsaa_ratio_y = 1
hdr_settings[__HDR_FXAA__].use_post_aa = 2

hdr_settings[__HDR_2xSSAA_FXAA__] = {}
hdr_settings[__HDR_2xSSAA_FXAA__].value = __HDR_2xSSAA_FXAA__
hdr_settings[__HDR_2xSSAA_FXAA__].text = "2S+F"
hdr_settings[__HDR_2xSSAA_FXAA__].fsaa_ratio_x = 1
hdr_settings[__HDR_2xSSAA_FXAA__].fsaa_ratio_y = 2
hdr_settings[__HDR_2xSSAA_FXAA__].use_post_aa = 2

hdr_settings[__HDR_4xSSAA__] = {}
hdr_settings[__HDR_4xSSAA__].value = __HDR_4xSSAA__
hdr_settings[__HDR_4xSSAA__].text = "4S"
hdr_settings[__HDR_4xSSAA__].fsaa_ratio_x = 2
hdr_settings[__HDR_4xSSAA__].fsaa_ratio_y = 2
hdr_settings[__HDR_4xSSAA__].use_post_aa = 0

hdr_settings[__HDR_4xSSAA_FXAA__] = {}
hdr_settings[__HDR_4xSSAA_FXAA__].value = __HDR_4xSSAA_FXAA__
hdr_settings[__HDR_4xSSAA_FXAA__].text = "4S+F"
hdr_settings[__HDR_4xSSAA_FXAA__].fsaa_ratio_x = 2
hdr_settings[__HDR_4xSSAA_FXAA__].fsaa_ratio_y = 2
hdr_settings[__HDR_4xSSAA_FXAA__].use_post_aa = 2

hdr_settings[__HDR_8xSSAA_FXAA__] = {}
hdr_settings[__HDR_8xSSAA_FXAA__].value = __HDR_8xSSAA_FXAA__
hdr_settings[__HDR_8xSSAA_FXAA__].text = "8S+F"
hdr_settings[__HDR_8xSSAA_FXAA__].fsaa_ratio_x = 2
hdr_settings[__HDR_8xSSAA_FXAA__].fsaa_ratio_y = 4
hdr_settings[__HDR_8xSSAA_FXAA__].use_post_aa = 2

local hdr_setting_value = 0
local hdr_setting_value_min = CONFIG.HDR.MIN_VALUE
local hdr_setting_value_max = CONFIG.HDR.MAX_VALUE

if CONFIG.HDR.MAX_VALUE < CONFIG.HDR.MIN_VALUE then
	-- handles an error in .ini file format version "1", where these values were reversed
	local hdr_setting_value_min = CONFIG.HDR.MAX_VALUE
	local hdr_setting_value_max = CONFIG.HDR.MIN_VALUE
end

if CONFIG.HDR.SET_INITIAL_VALUE then
	
	hdr_setting_value = CONFIG.HDR.INITIAL_VALUE
	push_hdr_settings()

else

	if fsaa_ratio_x == 1 and fsaa_ratio_y == 1 and use_post_aa == 2 then
		hdr_setting_value = __HDR_FXAA__
	elseif fsaa_ratio_x == 1 and fsaa_ratio_y == 2 and use_post_aa == 2 then
		hdr_setting_value = __HDR_2xSSAA_FXAA__
	elseif fsaa_ratio_x == 2 and fsaa_ratio_y == 2 and use_post_aa == 0 then
		hdr_setting_value = __HDR_4xSSAA__
	elseif fsaa_ratio_x == 2 and fsaa_ratio_y == 2 and use_post_aa == 2 then
		hdr_setting_value = __HDR_4xSSAA_FXAA__
	elseif fsaa_ratio_x == 2 and fsaa_ratio_y == 4 and use_post_aa == 2 then
		hdr_setting_value = __HDR_8xSSAA_FXAA__
	else
		hdr_setting_value = __HDR_NONE__
	end

end
local lod_hdr_time_last_check_up = os.clock()
local lod_hdr_time_last_check_down = os.clock()

local display_text = ""

-----------------------------------------------------------------------------
function lod_show_display() 

	if lod_show_window == false then
		return
	end

	XPLMSetGraphicsState(0,0,0,1,1,0,0)

	if lod_control_mode_auto == true then
		graphics.set_color(1.0, 0.0, 0.0, 0.5)		
		graphics.draw_rectangle(displays[__DISPLAY_FPS__].left, displays[__DISPLAY_FPS__].bottom, displays[__DISPLAY_FPS__].right - 1, displays[__DISPLAY_FPS__].top - 1)
	end

	graphics.set_color(1.0, 1.0, 1.0, 1.0)
	-- boxes
	graphics.draw_line(displays[1].left, displays[1].bottom, displays[1].right, displays[1].bottom)
	graphics.draw_line(displays[1].right, displays[1].bottom, displays[1].right, displays[display_stop_at].top)
	graphics.draw_line(displays[display_stop_at].right, displays[display_stop_at].top, displays[display_stop_at].left, displays[display_stop_at].top)
	graphics.draw_line(displays[display_stop_at].left, displays[display_stop_at].top, displays[display_stop_at].left, displays[1].bottom)
	-- hr separators
	for i = 1, display_stop_at - 1 do
		graphics.draw_line(displays[i].left, displays[i].top, displays[i].right, displays[i].top)	
	end
		
	--if 1 / frame_rate_period < CONFIG.LOD.FPS_MIN then graphics.set_color(0.0, 0.0, 0.0, 1.0) else graphics.set_color(0.0, 1.0, 0.0, 1.0) end
	--graphics.draw_rectangle(lod_posX_right - 6, lod_posY_middle2 + 6, lod_posX_right - 3, lod_posY_top - 6)

	for i = 1, display_stop_at do
	
		if i == __DISPLAY_LOD__ then

			if hdr_post_aa_only then
		
				if displays[i].index == __DISPLAY_LOD_MODE_LOD_HDR__ then

					if use_post_aa == 0 then
						hdr_post_aa_text = ''
					else
						hdr_post_aa_text = 'AA'
					end
				
					display_text = displays[i][__DISPLAY_LOD_MODE_LOD_HDR__].caption .. string.format("%1.1f", lod_bias_rat) .. '/' .. hdr_post_aa_text
				
				elseif displays[i].index == __DISPLAY_LOD_MODE_LOD__ then
					display_text = displays[i][__DISPLAY_LOD_MODE_LOD__].caption .. string.format("%1.1f", lod_bias_rat)

				elseif displays[i].index == __DISPLAY_LOD_MODE_HDR__ then

					if use_post_aa == 0 then
						hdr_post_aa_text = 'Off'
					else
						hdr_post_aa_text = 'On'
					end
					display_text = displays[i][__DISPLAY_LOD_MODE_HDR__].caption .. hdr_post_aa_text
			
				end

			else
			
				if displays[i].index == __DISPLAY_LOD_MODE_LOD_HDR__ then
					display_text = displays[i][__DISPLAY_LOD_MODE_LOD_HDR__].caption .. string.format("%1.1f", lod_bias_rat) .. '/' .. hdr_settings[hdr_setting_value].text
				
				elseif displays[i].index == __DISPLAY_LOD_MODE_LOD__ then
					display_text = displays[i][__DISPLAY_LOD_MODE_LOD__].caption .. string.format("%1.1f", lod_bias_rat)

				elseif displays[i].index == __DISPLAY_LOD_MODE_HDR__ then
					display_text = displays[i][__DISPLAY_LOD_MODE_HDR__].caption .. hdr_settings[hdr_setting_value].text
			
				end
		
			end
			
		elseif i == __DISPLAY_INFO__ then
		
			if displays[i].index == __DISPLAY_INFO_MODE_AVERAGE_FPS__ then
				display_text = displays[i][__DISPLAY_INFO_MODE_AVERAGE_FPS__].caption .. string.format("%2.1f", lod_fps_average)
			
			elseif displays[i].index == __DISPLAY_INFO_MODE_FOG_BE_GONE__ then
				display_text = displays[i][__DISPLAY_INFO_MODE_FOG_BE_GONE__].caption .. string.format("%2.1f", fog_be_gone)
					
			elseif displays[i].index == __DISPLAY_INFO_MODE_MAX_DSF_VIS__ then
				display_text = displays[i][__DISPLAY_INFO_MODE_MAX_DSF_VIS__].caption .. string.format("%3i", max_dsf_vis_ever / 1000) .. 'K'
			end

		elseif i == __DISPLAY_FPS__ then
			display_text = "FPS: " .. string.format("%2.0f", 1 / frame_rate_period)
		
		elseif i == __DISPLAY_TITLE__ then			
			display_text = app['caption'] .. ' ' .. app['version']
			
		end
		
		draw_string_Helvetica_10(displays[i].left + 3, displays[i].bottom + 3, display_text)
		
	end
		
	if CONFIG.DEBUG.SHOW then	
		debug_show_value(0, "View Feet: " .. string.format("%6i", view_y_meters * __FEET_2_METERS__))
		debug_show_value(1, "Visibility: " .. string.format("%6i", visibility_reported_m))
		debug_show_value(2, "MinDsfVis: " .. string.format("%6i", min_dsf_vis_ever))
		debug_show_value(3, "MaxDsfVis: " .. string.format("%6i", max_dsf_vis_ever))
		debug_show_value(4, "FogBeGone: " .. string.format("%2.1f", fog_be_gone))
	end
  
end

-----------------------------------------------------------------------------
function debug_show_value(idx, display_text)
	draw_string_Helvetica_10(displays[1].left + 3, displays[__DISPLAY_MAX__].top + 30 + idx * 12, display_text)
end

-----------------------------------------------------------------------------
function lod_calc_average()

	if is_sim_paused == 0 then 
		
		lod_fps_average = ((lod_fps_average * lod_fps_average_counter) + (1 / frame_rate_period)) / (lod_fps_average_counter + 1)
		lod_fps_average_counter = lod_fps_average_counter + 1
		
		if lod_fps_average_counter > CONFIG.LOD.FPS_AVERAGE_TIMEFRAME_SECONDS then lod_fps_average_counter = CONFIG.LOD.FPS_AVERAGE_TIMEFRAME_SECONDS end
		
	end

end

-----------------------------------------------------------------------------
function lod_main() 

	lod_calc_average()
	
	if lod_control_mode_auto == true and is_sim_paused == 0 then

		if os.clock() > lod_time + CONFIG.LOD.FPS_CHECK_SECONDS then

			if hgt_m > lod_agl_threshold then 
			
				if lod_bias_rat_below_agl_threshold == -1 then
				
					lod_bias_rat_below_agl_threshold = lod_bias_rat
					lod_bias_rat = 9.9 
					
				end
				
			else
			
				if lod_bias_rat_below_agl_threshold > -1 then 
					
					lod_bias_rat = lod_bias_rat_below_agl_threshold
					lod_bias_rat_below_agl_threshold = -1
				
				end
					
				local lod_fps = 1 / frame_rate_period
				
				lod_last_direction = ""

				--if lod_bias_rat > CONFIG.LOD.MAX_VALUE and lod_fps_average > CONFIG.LOD.FPS_MAX then 			
				if lod_bias_rat > CONFIG.LOD.MAX_VALUE and lod_fps > CONFIG.LOD.FPS_MAX then 
				
					lod_bias_rat = lod_bias_rat - 0.1 
					lod_last_direction = "-"

					if lod_bias_rat < CONFIG.LOD.MAX_VALUE then lod_bias_rat = CONFIG.LOD.MAX_VALUE end
					
				--elseif lod_bias_rat < CONFIG.LOD.MIN_VALUE and lod_fps_average < CONFIG.LOD.FPS_MIN then 
				elseif lod_bias_rat < CONFIG.LOD.MIN_VALUE and lod_fps < CONFIG.LOD.FPS_MIN then 
				
					lod_bias_rat = lod_bias_rat + 0.1 
					lod_last_direction = "+"

					if lod_bias_rat > CONFIG.LOD.MIN_VALUE then lod_bias_rat = CONFIG.LOD.MIN_VALUE end
					
				-- if lod_fps_average > CONFIG.LOD.FPS_MIN + 1 and lod_fps > CONFIG.LOD.FPS_MIN then
				-- if lod_fps_average > lod_fps_down_threshold and lod_fps > CONFIG.LOD.FPS_MIN then
				elseif lod_bias_rat > CONFIG.LOD.MAX_VALUE and lod_fps_average > lod_fps_down_threshold and lod_fps > lod_fps_average then
				
					lod_bias_rat = lod_bias_rat - ((lod_fps_average - CONFIG.LOD.FPS_MIN) / 200)
					lod_last_direction = "-"
					if lod_bias_rat < CONFIG.LOD.MAX_VALUE then lod_bias_rat = CONFIG.LOD.MAX_VALUE end
					
				end
			
			end

			lod_time = os.clock()

		end

		if CONFIG.HDR.CHANGE then

			if hdr_post_aa_only then
			
				if lod_fps_average < CONFIG.LOD.FPS_MIN and use_post_aa == 2 then 
					use_post_aa = 0
				elseif lod_fps_average > CONFIG.LOD.FPS_MAX and use_post_aa == 0 then 
					use_post_aa = 2
				end
					
			else
		
				if lod_fps_average > CONFIG.HDR.FPS_THRESHOLD_DOWN then 
					lod_hdr_time_last_check_down = os.clock()
				elseif lod_fps_average < CONFIG.HDR.FPS_THRESHOLD_UP then 
					lod_hdr_time_last_check_up = os.clock() 
				end
							
				if os.clock() > lod_hdr_time_last_check_down + CONFIG.HDR.WAIT_TIME_SECONDS_DOWN then
				
					if lod_fps_average < CONFIG.HDR.FPS_THRESHOLD_DOWN and lod_bias_rat >= CONFIG.HDR.LOD_THRESHOLD_BEFORE_LOWERING_HDR then

						if hdr_setting_value > hdr_setting_value_min then
							hdr_setting_value = hdr_setting_value - 1
							push_hdr_settings()
						end
					
					end

					lod_hdr_time_last_check_down = os.clock()
					lod_hdr_time_last_check_up = os.clock()
					
				end
			
				if os.clock() > lod_hdr_time_last_check_up + CONFIG.HDR.WAIT_TIME_SECONDS_UP then
				
					if lod_fps_average > CONFIG.HDR.FPS_THRESHOLD_UP then
					
						if hdr_setting_value < hdr_setting_value_max then
							hdr_setting_value = hdr_setting_value + 1
							push_hdr_settings()
						end
									
					end

					lod_hdr_time_last_check_up = os.clock()

				end

			end
				
		end
		
	end
	
	if lod_bias_rat > 9.9 then 
		lod_bias_rat = 9.9
	elseif lod_bias_rat < 0 then 
		lod_bias_rat = 0 
	end
	  
end

-----------------------------------------------------------------------------
function push_hdr_settings()

	fsaa_ratio_x = hdr_settings[hdr_setting_value].fsaa_ratio_x
	fsaa_ratio_y = hdr_settings[hdr_setting_value].fsaa_ratio_y
	use_post_aa = hdr_settings[hdr_setting_value].use_post_aa

end

-----------------------------------------------------------------------------
function lod_mouse_wheel_events()  

	if is_inside_box(MOUSE_X, MOUSE_Y, displays[__DISPLAY_LOD__]) then
	
		if displays[__DISPLAY_LOD__].index == __DISPLAY_LOD_MODE_LOD__ then
			lod_bias_rat = lod_bias_rat - (MOUSE_WHEEL_CLICKS / 10)
		
		elseif displays[__DISPLAY_LOD__].index == __DISPLAY_LOD_MODE_HDR__ then
		
			if MOUSE_WHEEL_CLICKS > 0 then
				hdr_setting_value = hdr_setting_value + 1
				if hdr_setting_value > __HDR_8xSSAA_FXAA__ then hdr_setting_value = __HDR_8xSSAA_FXAA__ end
			else
				hdr_setting_value = hdr_setting_value - 1
				if hdr_setting_value < __HDR_NONE__ then hdr_setting_value = __HDR_NONE__ end
			end
		
		end
	 
		RESUME_MOUSE_WHEEL = true
  
	end

end

-----------------------------------------------------------------------------
function is_inside_box(x, y, box)
	return x > box.left and x < box.right and y > box.bottom and y < box.top
end

-----------------------------------------------------------------------------
function lod_mouse_click_events() 

	if MOUSE_STATUS == "down" and is_inside_box(MOUSE_X, MOUSE_Y, displays[__DISPLAY_TITLE__]) then
		if display_stop_at == __DISPLAY_FPS__ then 
			display_stop_at = __DISPLAY_MAX__
		else
			display_stop_at =__DISPLAY_FPS__
		end
	end
	
	if MOUSE_STATUS == "down" and is_inside_box(MOUSE_X, MOUSE_Y, displays[__DISPLAY_FPS__]) then
		lod_control_mode_auto = not lod_control_mode_auto
	end
  
	if MOUSE_STATUS == "down" and is_inside_box(MOUSE_X, MOUSE_Y, displays[__DISPLAY_LOD__]) then
		displays[__DISPLAY_LOD__].index = displays[__DISPLAY_LOD__].index + 1
		if displays[__DISPLAY_LOD__].index == __DISPLAY_LOD_MODE_MAX__ + 1 then 
			displays[__DISPLAY_LOD__].index = __DISPLAY_LOD_MODE_MIN__ 
		end
	end

	if MOUSE_STATUS == "down" and is_inside_box(MOUSE_X, MOUSE_Y, displays[__DISPLAY_INFO__]) then
		displays[__DISPLAY_INFO__].index = displays[__DISPLAY_INFO__].index + 1
		if displays[__DISPLAY_INFO__].index == __DISPLAY_INFO_MODE_MAX__ + 1 then 
			displays[__DISPLAY_INFO__].index = __DISPLAY_INFO_MODE_MIN__ 
		end
	end
	
end

-----------------------------------------------------------------------------
function Set_DSF_Visibility()

	if xE_is_installed then
		if xE_is_active == 1 then
			return
		end		
	end

	local hgt_ft
	hgt_ft = view_y_meters * __FEET_2_METERS__
	if hgt_ft < 1 then hgt_ft = 1 end
	
	if hgt_ft < 30000 then
		max_dsf_vis_ever = math.min(CONFIG.DSF_VISIBILITY.DSF_VIS_EVER_THRESHOLD, (hgt_ft + visibility_reported_m) * 5)
	else
		max_dsf_vis_ever = CONFIG.DSF_VISIBILITY.DSF_VIS_EVER_THRESHOLD
	end

	if CONFIG.FOG_BE_GONE.CHANGE then
		Set_Fog_Be_Gone(hgt_ft)
	end
	
end

-----------------------------------------------------------------------------
function Set_Fog_Be_Gone(hgt_ft)

	local next_fog_be_gone = (5000 / visibility_reported_m * (hgt_ft / 3000)) - (4000 / math.max(100, hgt_ft))

	if next_fog_be_gone < CONFIG.FOG_BE_GONE.MIN_VALUE then next_fog_be_gone = CONFIG.FOG_BE_GONE.MIN_VALUE end
	if next_fog_be_gone > CONFIG.FOG_BE_GONE.MAX_VALUE then next_fog_be_gone = CONFIG.FOG_BE_GONE.MAX_VALUE end
	
	fog_be_gone = next_fog_be_gone
	
end

-----------------------------------------------------------------------------
if CONFIG.FOG_BE_GONE.SET_INITIAL_VALUE then
	fog_be_gone = CONFIG.FOG_BE_GONE.INITIAL_VALUE
end

create_command("FlyWithLua/" .. app['name'] .. "/switch_Mode", "Switch auto/manual", "lod_control_mode_auto = not lod_control_mode_auto", "", "")
create_command("FlyWithLua/" .. app['name'] .. "/show_hide", "Show/Hide window", "lod_show_window = not lod_show_window", "", "")

add_macro(app['name'] .. "Show Hide window", "lod_show_window = not lod_show_window")

-----------------------------------------------------------------------------
do_on_mouse_click("lod_mouse_click_events()")
do_on_mouse_wheel("lod_mouse_wheel_events()")

do_often("lod_main()")
do_often("lod_calc_average()")

do_every_draw("lod_show_display()")

if CONFIG.DSF_VISIBILITY.CHANGE then
	do_often("Set_DSF_Visibility()")
end

--DataRef("sam_override", "sam/seasons/override", "writable")